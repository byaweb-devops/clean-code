# Clean Code

## PHP CS FIXER

### Install PHP CS FIXER

    $ composer global require friendsofphp/php-cs-fixer

    # To use binaries that you have installed globally through composer on your command line, you need add the path to these to your $PATH variable. To do so add the following to your ~/.bashrc or ~/.zshrc file.

    # add composer to path
    if [ -d "$HOME/.config/composer/vendor/bin" ] ; then
        PATH="$HOME/.config/composer/vendor/bin:$PATH"
    fi 

    # To verify that everything worked you can run the following.
    $ php-cs-fixer --version

### Install PHP CS FIXER Extension on vscode

Next you will need to install the extension **junstyle.php-cs-fixer** to vscode. Using the extensions tab in vscode you can search for php-cs-fixer and click install.

### Configuration

The last thing you need to do is to create **.php-cs-fixer.php** configuration file so you can set up the code style rules you want to use. You can use the example available on this repository. Just download it and save it under **~/.php-cs-fixer.php**.

Now make sure that you have Format on save enabled in vscode and start formatting your code on the fly.

## PHP STAN

### Install PHP STAN

    $ composer global require phpstan/phpstan

    # To verify that everything worked you can run the following.
    $ phpstan --version

    # Example of use
    $ phpstan analyse --level=7 --autoload-file=/PATH/TO/vendor/autoload.php /PATH/TO/someone.php

### Install PHP STAN Extension on vscode

Next you will need to install the extension **breezelin.phpstan** to vscode. Using the extensions tab in vscode you can search for PHP Static Analysis and click install.
